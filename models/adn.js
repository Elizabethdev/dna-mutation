const mongoose = require('mongoose');

const DnaSchema = mongoose.Schema({
  adn:{
    type: String,
    unique: true
  },
  has_mutation: {
    type: Boolean,
    require: true,
  },
  created_at: {
    type: Date,
    default: Date.now()
  }

});

module.exports = mongoose.model('Dna', DnaSchema);