const mongoose = require('mongoose');

const AppSchema = mongoose.Schema({
  name: {
    type: String,
    require: true,
    trim: true,
    unique: true
  },
  created_at: {
    type: Date,
    default: Date.now()
  }

});

module.exports = mongoose.model('App', AppSchema);