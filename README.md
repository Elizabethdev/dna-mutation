# dna-mutation

API REST desarrollada en express y nodejs para detectar si una persona tiene diferencias genéticas basándose en
su secuencia de ADN.

**Sin Mutación**
| A | T | G | C | G | A |
| - | - | - | - | - | - |
| C | A | G | T | G | C |
| T | T | A | T | T | T |
| A | G | A | C | G | G |
| G | C | G | T | C | A |
| T | C | A | C | T | G |

**Con Mutación**
| A | T | G | C | G | A |
| - | - | - | - | - | - |
| C | A | G | T | G | C |
| T | T | A | T | G | T |
| A | G | A | A | G | G |
| C | C | C | C | T | A |
| T | C | A | G | T | G |

Sabrás si existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua,
horizontal o vertical.

# Endpoints: 

* POST:```https://node-adn.herokuapp.com/api/auth ``` -> Obtener un token y autorizar a la aplicación que va a acceder al endpoint.
Enviar un JSON el cual tenga el siguiente formato: 
```sh
{
	"name": "POSTMAN"
}
```
Copiar el token que devuelve y enviarlo en el ```headers: 'x-auth-token'``` de los endpoints: ```POST: api/mutation y  GET: api/status```.

* POST:```https://node-adn.herokuapp.com/api/mutation ```-> Devuelve 200 o 403 si encuentra una mutacion o no. Enviar un JSON el cual tenga el siguiente formato:
```sh
{
 "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```
* GET: ```https://node-adn.herokuapp.com/api/status ``` ->  Devuelve un JSON con las estadísticas de las verificaciones de ADN:
```sh
{
 "count_mutations": 40,
 "count_no_mutation": 100,
 "ratio": 0.4
}

```

# Uso local: 

## Endpoints locales

* POST: [http://localhost:4000/api/auth](http://localhost:4000/api/auth) 
* POST: [http://localhost:4000/api/mutation](http://localhost:4000/api/mutation) 
* GET: [http://localhost:4000/api/status](http://localhost:4000/api/status) 

Para ejecutar la api de manera local seguir los siguientes pasos:

**Nota: tener instalado node js ^14.15.4.

Descargar o clonar el repositorio: `https://gitlab.com/Elizabethdev/dna-mutation`

Ejecutar en consola:
```sh
`npm install`

Ejecutar la aplicación en modo desarrollo
`npm run dev`
```

** Importante tener el archivo ejemplo.env como:
```.env``` y colocar la cadena de conexion a la Base de datos.

## MongoDB: 

 ```sh
 Base de datos en MongoDB Atlas, 
    cambiar la variable 'DB_MONGO' actual por DB_MONGO=mongodb://localhost:27017/tubdlocal, 
    para pruebas en local con mongoDB compass.

    Modelos: 
        catalogos.dnas: {
            adn:{
                type: String,
                unique: true
            },
            has_mutation: {
                type: Boolean,
                require: true,
            },
            created_at: {
                type: Date,
                default: Date.now()
            }
        },
        catalogos.apps: {
            name: {
                type: String,
                require: true,
                trim: true,
                unique: true
            },
            created_at: {
                type: Date,
                default: Date.now()
            }
        }
```

