const mongoose = require ('mongoose');

const conectarBD = async () => {
    
	try {
		await mongoose.connect(process.env.DB_MONGO, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true
		});
	} catch (error) {
		process.exit(1); //detener la app
	}
}

module.exports = conectarBD;