
const toArrayOfStrings = (matrix) => {
  return matrix.map((array) => {
    return array.join('');
  });
}

module.exports = toArrayOfStrings
