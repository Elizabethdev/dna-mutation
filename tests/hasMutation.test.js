const {hasMutation} = require('../utils/hasMutation');

describe('DNA sequence mutation', () => {
  it('should be true horizontal mutation', () => {
    expect(hasMutation(['AAAAAA', 'CAGTGC', 'TTTTTT', 'AGACGG', 'CCCCCA', 'TGGGGG'])).toBe(true)
  })

  it('should be true vertical mutation', () => {
    expect(hasMutation(['ATGCGA', 'ACGTGA', 'ATATTA', 'AGACGA', 'GCGTCC', 'TCACTG'])).toBe(true)
  })

  it('should be true oblique mutation', () => {
    expect(hasMutation(['AGGCGA', 'CAGTGC', 'TTAGTT', 'ATAAGG', 'GCTTCA', 'TCATTG'])).toBe(true)
  })

  it('should be true inverted oblique mutation test', () => {
    expect(hasMutation(['ATGCGA', 'CAGGAC', 'TTGACT', 'AGACGG', 'GACTCA', 'TCGCTG'])).toBe(true)
  })

  it('should be true mixed mutation test', () => {
    expect(hasMutation(['AAAAGA', 'CAGGAC', 'TTGACT', 'TGTCGG', 'TACTCA', 'TCGCTG'])).toBe(true)
  })

  it('should be true without mutation', () => {
    expect(hasMutation(["TAATGA","CAGTGC","TTATGT","AGAAGG","CCTCTA","TCACTG"])).toBe(false)
  })
})
