require('dotenv').config();
const express = require('express');
const conectarDB = require('./config/db')

const app = express();

conectarDB();

app.use(express.json({extended: true}));

const port = process.env.PORT || 4000;

app.listen(port,'0.0.0.0', () => {
  console.log(`el servidor esta funcionando en el puerto ${port}`);
})

app.use('/api', require('./routes/api'));
app.use('/api', require('./routes/auth'));

