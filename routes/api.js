const express = require('express');
const router = express.Router();
const mutationController = require('../controllers/mutationController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

//api/mutation
router.post('/mutation', auth,
[
  check('dna.*').isString().withMessage('Invalid dna').custom(value => {
    if (value.match(/([^ACGT])/)) {
      return Promise.reject('Invalid dna')
    }
    return true;   
  }),
], 
  mutationController.Mutation    
);

//api/status
router.get('/status', auth,
  mutationController.Status    
);

module.exports = router;
