const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const authController = require('../controllers/authController');

//api/auth
router.post('/auth', 
  [
    check('name', 'Agrega nombre válido').isString()
	],
	authController.authApp
);

module.exports = router;