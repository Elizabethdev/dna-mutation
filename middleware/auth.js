const jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {
	const token = req.header('x-auth-token');

	if(!token) {
		return res.status(401).json({msg: 'Permiso denegado, no hay token'});
	}

	try {
		jwt.verify(token, process.env.FIRMA);
       
		// req.name = cifrado.appAuth.name;
        // console.log(cifrado, req.name)
		next();
	} catch (error) {
		res.status(401).json({msg: 'Token no válido'});
	}
}