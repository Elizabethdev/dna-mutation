const {hasMutation} = require('../utils/hasMutation');
const { validationResult  } = require('express-validator');
const Dna = require('../models/adn');

exports.Mutation = async (req, res) => {
	const errores = validationResult(req);

  if( !errores.isEmpty() ) {
    return res.status(400).json({errores:errores.array()})
  }

  const {dna} = req.body;

	try {
		let mutation = hasMutation(dna);
		let dnaExist = await Dna.findOne({ adn: JSON.stringify(dna) });
		let DNA =  new Dna();

		if(!dnaExist) {
      DNA.adn = JSON.stringify(dna);
			DNA.has_mutation = mutation;
			await DNA.save();
    }
		
		if(mutation) {
			return res.status(200).json();
		} 
		return res.status(403).json();
			
	} catch (error) {
		res.status(500).json({msg: 'Hubo un error'});
	}
}

exports.Status = async (req, res) => {
	try {
		
		let count_mutations = await Dna.countDocuments({ has_mutation: true });
		let count_no_mutations = await Dna.countDocuments({ has_mutation: false });
		let ratio = count_mutations/count_no_mutations;
		return res.status(200).json({count_mutations: count_mutations, count_no_mutations:count_no_mutations, ratio:ratio},);

	} catch (error) {
		res.status(500).json({msg: 'Hubo un error'});
	}
}
