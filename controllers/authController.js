const App = require('../models/authapp');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.authApp = async (req, res) => {
  const errores = validationResult(req);

  if( !errores.isEmpty() ) {
    return res.status(400).json({errores:errores.array()})
	}

	const {name} = req.body;

	try {
		let appAuth = await App.findOne({ name: name });
		if(!appAuth) {
      appAuth =  new App();
      appAuth.name = name;
			await appAuth.save();
		}
		//crear y firmar jwt
    const payload = {
      appAuth:{
        name: appAuth.name
      }
    };

    jwt.sign(payload, process.env.FIRMA, {
      expiresIn: 36000 //duracion del token en seg.
    }, (error, token) => {
      if(error) throw error;

      res.status(200).json({ token});
    });

	} catch (error) {
    console.log(error)
    res.status(500).json({msg: 'Hubo un error'});
	}
}
