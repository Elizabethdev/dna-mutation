const makeVertical = require('./makeVertical')
const makeOblique = require('./makeOblique')
const {makeObliqueInvert} = require('./makeObliqueInvert')

const findMutation = (dna) =>{
  
  let regex = /([ATGC])\1{3}/;

  let horizontal = dna.filter((string) => {
    return regex.test(string);
  });

  let vertical = makeVertical(dna).filter((string) => {
    return regex.test(string);
  });

  let oblique = makeOblique(dna).filter((string) => {
    return regex.test(string);
  });

  let obliqueInvert = makeObliqueInvert(dna).filter((string) => {
    return regex.test(string);
  });

  return [...horizontal,...vertical,...oblique,...obliqueInvert];

} 

const hasMutation = (dna) => {
  let found = findMutation(dna);
  return found.length > 1;
};

module.exports = {hasMutation};
