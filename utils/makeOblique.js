const toArrayOfStrings = require('../helpers/toArrayOfStrings')

const makeOblique = (dna) => {
	let index, col, row, obliqueDNA;
	let transformed = [];
	let cols = dna[0].length;
	let rows = dna.length;
	
	for (index = 0; index < rows; index++) {
		obliqueDNA = [];
		for(row = index, col = 0; row >= 0; row--, col++)
			obliqueDNA.push(dna[row][col]);
		transformed.push(obliqueDNA);
	}
	for (index = 1; index < cols; index++) {
		obliqueDNA = [];
		for(row = rows - 1, col = index; col < cols; row--, col++)
			obliqueDNA.push(dna[row][col]);
		transformed.push(obliqueDNA);
	}

  return toArrayOfStrings(transformed);
}

module.exports = makeOblique
