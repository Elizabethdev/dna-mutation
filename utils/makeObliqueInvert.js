const isOblique = require('./makeOblique')

const reverseString = (string) => {
  return string.split("").reverse().join("");
}

const  reverseMatrix = (dna) => {
  return dna.map((string) => {
    return reverseString(string);
  });
}
const makeObliqueInvert = (dna) => {
  let reverse = reverseMatrix(dna);
  return isOblique(reverse);
}

module.exports = {makeObliqueInvert}



