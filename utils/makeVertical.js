const toArrayOfStrings = require('../helpers/toArrayOfStrings')

const makeVertical = (dna) => {
  let col, row, verticalDNA;
  let transformed = [];
  let cols = dna[0].length;
	let rows = dna.length;

  for (col = 0; col < cols; col++) {
    verticalDNA = [];
    for( row = 0; row < rows;  row++)
      verticalDNA.push(dna[row][col]);
    transformed.push(verticalDNA);
  }
  return toArrayOfStrings(transformed);
}

module.exports = makeVertical
